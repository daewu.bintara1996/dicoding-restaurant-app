import 'package:restaurant_app/models/consumer_review.dart';

import 'restaurant.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class BaseData {
  bool status;
  bool error;
  String text;
  String message;
  int founded;
  List<Restaurant> restaurants;
  Restaurant restaurant;
  List<ConsumerReview> customerReviews;

  BaseData(
      {this.restaurants,
      this.restaurant,
      this.text = "",
      this.status = false,
      this.error,
      this.message = "",
      this.customerReviews,
      this.founded});

  BaseData.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    founded = json['founded'];
    if (json['restaurants'] != null) {
      restaurants = List<Restaurant>();
      json['restaurants'].forEach((v) {
        restaurants.add(Restaurant.fromJson(v));
      });
    }
    message = json['message'];
    restaurant = json['restaurant'] != null
        ? new Restaurant.fromJson(json['restaurant'])
        : null;
    if (json['customerReviews'] != null) {
      customerReviews = List<ConsumerReview>();
      json['customerReviews'].forEach((v) {
        customerReviews.add(ConsumerReview.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['error'] = this.error;
    data['founded'] = this.founded;
    if (this.restaurants != null) {
      data['restaurants'] = this.restaurants.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    if (this.restaurant != null) {
      data['restaurant'] = this.restaurant.toJson();
    }
    if (this.customerReviews != null) {
      data['customerReviews'] =
          this.customerReviews.map((v) => v.toJson()).toList();
    }
    return data;
  }
}