///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class Item {
  var name;

  Item({this.name});

  Item.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = this.name;
    return data;
  }
}