import 'consumer_review.dart';
import 'item.dart';
import 'menu.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class Restaurant {
  var id;
  var name;
  var description;
  var pictureId;
  var city;
  var rating;
  Menu menus;
  List<Item> categories;
  List<ConsumerReview> consumerReviews;

  Restaurant(
      {this.id,
        this.name,
        this.description,
        this.pictureId,
        this.city,
        this.categories,
        this.rating,
        this.menus,
        this.consumerReviews});

  Restaurant.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    pictureId = json['pictureId'];
    city = json['city'];
    rating = json['rating'];
    menus = json['menus'] != null ? Menu.fromJson(json['menus']) : null;
    if (json['categories'] != null) {
      categories = List<Item>();
      json['categories'].forEach((v) {
        categories.add(Item.fromJson(v));
      });
    }
    if (json['customerReviews'] != null) {
      consumerReviews = List<ConsumerReview>();
      json['customerReviews'].forEach((v) {
        consumerReviews.add(ConsumerReview.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['pictureId'] = this.pictureId;
    data['city'] = this.city;
    data['rating'] = this.rating;
    if (this.menus != null) {
      data['menus'] = this.menus.toJson();
    }
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    if (this.consumerReviews != null) {
      data['customerReviews'] =
          this.consumerReviews.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
