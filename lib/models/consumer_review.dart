///
/// createdby Daewu Bintara
/// Friday, 30/10/20
///

class ConsumerReview {
  String name;
  String review;
  String date;

  ConsumerReview({this.name, this.review, this.date});

  ConsumerReview.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    review = json['review'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['review'] = this.review;
    data['date'] = this.date;
    return data;
  }
}