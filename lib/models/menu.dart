import 'item.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class Menu {
  List<Item> foods;
  List<Item> drinks;

  Menu({this.foods, this.drinks});

  Menu.fromJson(Map<String, dynamic> json) {
    if (json['foods'] != null) {
      foods = List<Item>();
      json['foods'].forEach((v) {
        foods.add(Item.fromJson(v));
      });
    }
    if (json['drinks'] != null) {
      drinks = List<Item>();
      json['drinks'].forEach((v) {
        drinks.add(Item.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.foods != null) {
      data['foods'] = this.foods.map((v) => v.toJson()).toList();
    }
    if (this.drinks != null) {
      data['drinks'] = this.drinks.map((v) => v.toJson()).toList();
    }
    return data;
  }
}