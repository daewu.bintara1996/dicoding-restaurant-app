import 'package:flutter/material.dart';
import 'package:restaurant_app/custom/constants.dart';
import 'package:restaurant_app/routes/routes.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    _check(context);
    
    return Scaffold(
      body: Center(
        child: Image.asset(ImageLogoAsset,
          height: 100,
          width: 100,
        ),
      ),
    );
  }

  void _check(BuildContext context) async {
    await Future.delayed(Duration(seconds: 3),(){});
    Navigator.pushReplacementNamed(context, RouterName.restaurant_list);
  }
}
