import 'package:flutter/material.dart';
import 'package:restaurant_app/base/api_service.dart';
import 'package:restaurant_app/custom/app_themes.dart';
import 'package:restaurant_app/custom/constants.dart';
import 'package:restaurant_app/models/base_data.dart';
import 'package:restaurant_app/modules/restaurant_list/widgets/restaurant_item_widget.dart';
import 'package:restaurant_app/routes/routes.dart';
import 'package:restaurant_app/utils/widgets/my_widgets.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class SearchRestaurantScreen extends StatefulWidget {
  @override
  _SearchRestaurantScreenState createState() => _SearchRestaurantScreenState();
}

class _SearchRestaurantScreenState extends State<SearchRestaurantScreen> {
  BaseData _baseData = BaseData();
  TextEditingController _textEditingController = TextEditingController();
  String _keyword = "";

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  _loadData() async {
    _baseData = await ApiService().getData(endPoint: "search", params: {
      "q": _keyword
    });
    if(_baseData.founded == 0) {
      _baseData.text = "No Data";
      _baseData.message = "No Data";
    }
    setState(() {});
  }

  AppBar _searchBar() {
    return AppBar(
      backgroundColor: AppThemes.lightTheme.backgroundColor,
      iconTheme: IconThemeData(color: Colors.grey[700]),
      brightness: Brightness.light,
      title: TextFormField(
        controller: _textEditingController,
        autofocus: true,
        onChanged: (val){
          setState(() {
            _baseData.status = false;
          });
          _keyword = val;
          _loadData();
        },
        textInputAction: TextInputAction.search,
        onFieldSubmitted: (val){
          setState(() {
            _baseData.status = false;
          });
          _keyword = val;
          _loadData();
        },
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          hintText: "Search Restaurant",
          contentPadding: EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 10,
          ),
        ),
        // controller: ,
      ),
      actions: [
        IconButton(
          tooltip: "Clear search field",
          icon: Icon(Icons.close),
          color: Colors.grey[700],
          onPressed: (){
            _textEditingController.text = "";
            setState(() {
              _baseData.status = false;
            });
            _keyword = "";
            _loadData();
          },
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _searchBar(),
      body: _body(),
    );
  }

  _body() {

    if(!_baseData.status) {
      return MyWidgets.circleLoading();
    }

    if(_baseData.error) {
      return MyWidgets.textLabelled(text: _baseData.text);
    }

    if(_baseData.restaurants.length == 0) {
      return MyWidgets.textLabelled(text: _baseData.text);
    }

    return ListView.builder(
      padding: EdgeInsets.only(top: paddingL),
      itemCount: _baseData.restaurants.length,
      itemBuilder: (context, index){
        return RestaurantItemWidget(
          restaurant: _baseData.restaurants[index],
          onRestaurantPressed: (){
            Navigator.pushNamed(context, RouterName.restaurant_detail,
                arguments: _baseData.restaurants[index]);
          },
        );
      },
    );
  }
}
