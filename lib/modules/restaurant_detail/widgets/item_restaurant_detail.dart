import 'package:flutter/material.dart';
import 'package:restaurant_app/custom/app_themes.dart';
import 'package:restaurant_app/custom/constants.dart';
import 'package:restaurant_app/models/restaurant.dart';

import 'item_menu_fixed_widget.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

enum ItemRestaurantType {
  TITLE,
  DESCRIPTION,
  FOODS,
  DRINKS
}

class ItemRestaurantDetail extends StatelessWidget{

  final Restaurant restaurant;
  final ItemRestaurantType type;
  ItemRestaurantDetail({this.restaurant, this.type});
  BuildContext _context;

  _titleRestaurant() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: paddingL),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: paddingXL),
          Text('${restaurant.name}',
            style: AppThemes.lightTheme.textTheme.headline1.copyWith(
                color: Colors.black, fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(height: paddingS),
          Row(
            children: [
              Icon(Icons.location_on, color: Colors.grey, size: 16),
              SizedBox(width: paddingS),
              Text('${restaurant?.city}')
            ],
          ),
        ],
      ),
    );
  }

  _descriptionRestaurant() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: paddingL),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: paddingXL),
          Text('Description',
            style: AppThemes.lightTheme.textTheme.subtitle1.copyWith(
                color: Colors.black, fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(height: paddingM),
          Text('${restaurant.description}',
            style: AppThemes.lightTheme.textTheme.subtitle1.copyWith(
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  _restaurantFoods() {
    if(restaurant.menus.foods.length == 0){
      return Container();
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: paddingXL),
        Padding(
          padding: EdgeInsets.only(left: paddingL),
          child: Text('Restaurant Foods',
            style: AppThemes.lightTheme.textTheme.subtitle1.copyWith(
                color: Colors.black, fontWeight: FontWeight.bold
            ),
          ),
        ),
        SizedBox(height: paddingM),
        SizedBox(
          height: 160.0,
          child: ListView.builder(
              padding: EdgeInsets.only(left: paddingL),
              itemCount: restaurant.menus.foods.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index){
                return ItemMenuFixedWidget(
                  menuType: MenuType.FOOD,
                  restaurant: restaurant,
                  name: restaurant.menus.foods[index].name,
                );
              }
          ),
        ),
      ],
    );
  }

  _restaurantDrinks() {
    if(restaurant.menus.drinks.length == 0){
      return Container();
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: paddingXL),
        Padding(
          padding: EdgeInsets.only(left: paddingL),
          child: Text('Restaurant Drinks',
            style: AppThemes.lightTheme.textTheme.subtitle1.copyWith(
                color: Colors.black, fontWeight: FontWeight.bold
            ),
          ),
        ),
        SizedBox(height: paddingM),
        SizedBox(
          height: 160.0,
          child: ListView.builder(
            padding: EdgeInsets.only(left: paddingL),
            itemCount: restaurant.menus.drinks.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index){
              return ItemMenuFixedWidget(
                menuType: MenuType.DRINK,
                restaurant: restaurant,
                name: restaurant.menus.drinks[index].name,
              );
            }
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    this._context = context;
    switch(type){
      case ItemRestaurantType.TITLE :
        return _titleRestaurant();
        break;
      case ItemRestaurantType.DESCRIPTION :
        return _descriptionRestaurant();
        break;
      case ItemRestaurantType.FOODS :
        return _restaurantFoods();
        break;
      case ItemRestaurantType.DRINKS :
        return _restaurantDrinks();
        break;
      default:
        return Container();
        break;
    }
  }
}