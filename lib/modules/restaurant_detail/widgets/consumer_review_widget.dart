import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:restaurant_app/base/api_service.dart';
import 'package:restaurant_app/custom/app_themes.dart';
import 'package:restaurant_app/custom/constants.dart';
import 'package:restaurant_app/models/base_data.dart';
import 'package:restaurant_app/models/consumer_review.dart';
import 'package:restaurant_app/models/restaurant.dart';
import 'package:restaurant_app/utils/widgets/my_widgets.dart';

///
/// createdby Daewu Bintara
/// Friday, 30/10/20
///

class ConsumerReviewWidget extends StatefulWidget {
  final Restaurant restaurant;
  ConsumerReviewWidget({this.restaurant});

  @override
  _ConsumerReviewWidgetState createState() => _ConsumerReviewWidgetState();
}

class _ConsumerReviewWidgetState extends State<ConsumerReviewWidget> {

  TextEditingController _nameTextEditingController = TextEditingController();
  TextEditingController _reviewTextEditingController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey();
  bool _loadingToReview = false;

  @override
  Widget build(BuildContext context) {
    if(widget.restaurant.consumerReviews == null || widget.restaurant.consumerReviews.length == 0) {
      return Container();
    }
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: paddingL),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: paddingXL),
            Text('Customer Reviews',
              style: AppThemes.lightTheme.textTheme.subtitle1.copyWith(
                  color: Colors.black, fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(height: paddingM),
            ListView(
              padding: EdgeInsets.all(0),
              physics: ScrollPhysics(),
              shrinkWrap: true,
              children: widget.restaurant.consumerReviews.map((e) {
                return _itemRewier(e);
              }).toList(),
            ),
            _reviewRestaurant(),
            SizedBox(height: paddingXL),
          ],
        ),
      ),
    );
  }

  Widget _itemRewier(ConsumerReview e) {
    return Padding(
      padding: EdgeInsets.only(bottom: paddingM),
      child: Material(
        elevation: 1,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        child: Padding(
          padding: EdgeInsets.all(paddingM),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text("${e.name}",
                      style: AppThemes.lightTheme.textTheme.caption,
                    ),
                  ),
                  Text("${e.date}",
                    style: AppThemes.lightTheme.textTheme.caption,
                  ),
                ],
              ),
              SizedBox(height: paddingS),
              Text("${e.review}",
                style: AppThemes.lightTheme.textTheme.subtitle1,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _reviewRestaurant() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Expanded(
          child: Form(
            key: formKey,
            child: Column(
              children: [
                TextFormField(
                  controller: _nameTextEditingController,
                  validator: (val){
                    if(val == "" || val == null) {
                      return "Input your name";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    hintText: "Name",
                  ),
                ),
                TextFormField(
                  controller: _reviewTextEditingController,
                  validator: (val){
                    if(val == "" || val == null) {
                      return "Input your review";
                    }
                    return null;
                  },
                  maxLength: 255,
                  minLines: 2,
                  maxLines: 2,
                  decoration: InputDecoration(
                    hintText: "Review",
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(width: paddingM),
        Padding(
          padding: EdgeInsets.only(bottom: paddingL),
          child: _loadingToReview ? MyWidgets.circleLoading() :
          IconButton(
            tooltip: "Send your review",
            icon: Icon(Icons.send, color: PrimaryColor),
            onPressed: (){
              FocusScope.of(context).unfocus();
              log("NAME ==> ${_nameTextEditingController.text}");
              if(formKey.currentState.validate()) {
                _createReview();
              }
            },
          ),
        ),
      ],
    );
  }

  void _createReview() async {
    setState(() {
      _loadingToReview = true;
    });
    BaseData data = await ApiService().postData(endPoint: "review", params: {
      "id" : "${widget.restaurant.id}",
      "name" : _nameTextEditingController.text.toString(),
      "review" : _reviewTextEditingController.text.toString(),
    }, withToken: true);

    if(data.status) {
      widget.restaurant.consumerReviews = data.customerReviews;
      _nameTextEditingController.text = "";
      _reviewTextEditingController.text = "";
    } else {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text("${data.text}")));
    }
    _loadingToReview = false;
    setState(() {});
  }

}

