import 'package:flutter/material.dart';
import 'package:restaurant_app/custom/app_themes.dart';
import 'package:restaurant_app/custom/constants.dart';
import 'package:restaurant_app/models/restaurant.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

enum MenuType {
  FOOD,
  DRINK
}

class ItemMenuFixedWidget extends StatelessWidget {
  final Restaurant restaurant;
  final String name;
  final MenuType menuType;

  String _imgUrl = "https://3.bp.blogspot.com/-5AnR831Mt50/ToKO4InifXI/AAAAAAAAELw/aegw0BErfsk/w1200-h630-p-k-no-nu/Johnny%2BMemphis%2BSmokehouse%2BBarbeque%2B-%2BHot%2BLink%2BSandwich.jpg";

  ItemMenuFixedWidget({this.restaurant, this.name, this.menuType});

  @override
  Widget build(BuildContext context) {

    if(menuType == MenuType.DRINK) {
      _imgUrl = "https://upload.wikimedia.org/wikipedia/commons/4/45/A_small_cup_of_coffee.JPG";
    }

    return Container(
      margin: EdgeInsets.only(right: paddingL),
      height: 160,
      width: 160,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        child: Stack(
          children: [
            FadeInImage.assetNetwork(
                width: 160,
                height: 160,
                fit: BoxFit.cover,
                placeholder: "${ImagePlaceholderAsset}",
                image: "$_imgUrl"),
            Positioned(
              bottom: 0,
              right: 0,
              left: 0,
              child: Container(
                padding: EdgeInsets.all(paddingM),
                child: Text("$name",
                  style: AppThemes.lightTheme.textTheme.subtitle1.copyWith(
                    color: Colors.white
                  ),
                ),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[Colors.transparent, Colors.black54],
                  )
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
