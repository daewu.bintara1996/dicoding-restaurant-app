import 'package:flutter/material.dart';
import 'package:restaurant_app/base/api_service.dart';
import 'package:restaurant_app/custom/app_themes.dart';
import 'package:restaurant_app/custom/constants.dart';
import 'package:restaurant_app/models/base_data.dart';
import 'package:restaurant_app/models/restaurant.dart';
import 'package:restaurant_app/modules/restaurant_detail/widgets/consumer_review_widget.dart';
import 'package:restaurant_app/modules/restaurant_detail/widgets/item_restaurant_detail.dart';
import 'package:restaurant_app/utils/widgets/my_widgets.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class RestaurantDetailScreen extends StatefulWidget {
  Restaurant restaurant;
  RestaurantDetailScreen(this.restaurant);

  @override
  _RestaurantDetailScreenState createState() => _RestaurantDetailScreenState();
}

class _RestaurantDetailScreenState extends State<RestaurantDetailScreen> {
  Restaurant restaurant;
  BaseData _baseData = BaseData();

  @override
  void initState() {
    super.initState();
    restaurant = widget.restaurant;
    _loadData();
  }

  _loadData() async {
    restaurant.consumerReviews = [];
    _baseData = await ApiService().getData(endPoint: "detail/${restaurant.id}", params: {});
    restaurant = _baseData.restaurant;
    setState(() {});
  }

  Widget _restaurantImage() {
    return ClipRRect(
      borderRadius: BorderRadius.vertical(bottom: Radius.circular(15)),
      child: FadeInImage.assetNetwork(
        placeholder: "$ImagePlaceholderAsset",
        image: "$IMG_URL_LARGE${restaurant?.pictureId}",
        fit: BoxFit.cover,
        width: double.maxFinite,
        height: MediaQuery.of(context).size.height/2,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SliverAppBar(
                title: innerBoxIsScrolled ? Text("${restaurant.name}",
                  style: AppThemes.lightTheme.textTheme.headline2.copyWith(
                    color: Colors.black
                  ),
                ) : Container(),
                brightness: Brightness.light,
                floating: true,
                snap: true,
                pinned: true,
                expandedHeight: MediaQuery.of(context).size.height/2,
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(
                  color: Colors.black
                ),
                forceElevated: innerBoxIsScrolled,
                flexibleSpace: FlexibleSpaceBar(
                  collapseMode: CollapseMode.pin,
                  background: Hero(
                      tag: "${RESTAURANT_TAG}_${restaurant?.id}",
                      child: _restaurantImage()
                  ),
                  titlePadding: EdgeInsets.symmetric(horizontal: paddingL, vertical: paddingM),
                ),
              ),
            ),
          ];
        },
        body: _body(),
      ),
    );
  }

  _body() {

    if(!_baseData.status) {
      return MyWidgets.circleLoading();
    }

    if(_baseData.error) {
      return MyWidgets.textLabelled(text: _baseData.text);
    }

    return Builder(
      builder: (context){
        return CustomScrollView(
          slivers: [
            SliverOverlapInjector(handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context)),
            SliverList(delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
              switch (index) {
                case 0 :
                  return ItemRestaurantDetail(
                    restaurant: restaurant,
                    type: ItemRestaurantType.TITLE,
                  );
                  break;
                case 1 :
                  return ItemRestaurantDetail(
                    restaurant: restaurant,
                    type: ItemRestaurantType.DESCRIPTION,
                  );
                  break;
                case 2 :
                  return ItemRestaurantDetail(
                    restaurant: restaurant,
                    type: ItemRestaurantType.FOODS,
                  );
                  break;
                case 3 :
                  return ItemRestaurantDetail(
                    restaurant: restaurant,
                    type: ItemRestaurantType.DRINKS,
                  );
                  break;
                case 4 :
                  return ConsumerReviewWidget(
                    restaurant: restaurant,
                  );
                  break;
                default:
                  return Container();
                  break;
              }
            },
              childCount: 5,
            ))
          ],
        );
      },
    );
  }


}
