import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant_app/base/api_service.dart';
import 'package:restaurant_app/custom/app_themes.dart';
import 'package:restaurant_app/custom/constants.dart';
import 'package:restaurant_app/models/base_data.dart';
import 'package:restaurant_app/models/restaurant.dart';
import 'package:restaurant_app/modules/restaurant_list/widgets/restaurant_item_widget.dart';
import 'package:restaurant_app/routes/router_name.dart';
import 'package:restaurant_app/utils/widgets/my_widgets.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class RestaurantListScreen extends StatefulWidget {
  @override
  _RestaurantListScreenState createState() => _RestaurantListScreenState();
}

class _RestaurantListScreenState extends State<RestaurantListScreen> {
  BaseData _baseData = BaseData();
  @override
  void initState() {
    super.initState();
    _baseData.restaurants = [];
    _loadData();
  }
  
  _loadData() async {
    _baseData = await ApiService().getData(endPoint: "list", params: {});
    setState(() {});
  }

  SliverAppBar _appBar(bool innerBoxIsScrolled) {
    return SliverAppBar(
      actions: [
        IconButton(
          tooltip: "Search Restaurant",
          icon: Icon(Icons.search, color: Colors.grey[500]),
          onPressed: (){
            Navigator.pushNamed(context, RouterName.restaurant_search);
          },
        ),
      ],
      title: innerBoxIsScrolled ? Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Restaurant',
            style: AppThemes.lightTheme.textTheme.headline3.copyWith(
                fontWeight: FontWeight.bold,
            ),
          ),
          Text('Recomendation restaurant for you!',
            style: AppThemes.lightTheme.textTheme.caption,
          ),
        ],
      ) : Container(),
      brightness: Brightness.light,
      floating: true,
      snap: true,
      pinned: true,
      centerTitle: false,
      expandedHeight: 130.0,
      backgroundColor: AppThemes.lightTheme.backgroundColor,
      forceElevated: innerBoxIsScrolled,
      flexibleSpace: FlexibleSpaceBar(
        background: Center(
          child: Container(
            width: double.maxFinite,
            margin: EdgeInsets.only(top: paddingXXL),
            padding: EdgeInsets.symmetric(horizontal: paddingL),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Restaurant',
                  style: AppThemes.lightTheme.textTheme.headline1.copyWith(
                    fontWeight: FontWeight.bold, fontSize: 30
                  ),
                ),
                Text('Recomendation restaurant for you!',
                  style: AppThemes.lightTheme.textTheme.subtitle1,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: _appBar(innerBoxIsScrolled),
            ),
          ];
        },
        body: _body(),
      ),
    );
  }

  _body() {

    if(!_baseData.status) {
      return MyWidgets.circleLoading();
    }

    if(_baseData.error) {
      return MyWidgets.textLabelled(text: _baseData.text);
    }

    if(_baseData.restaurants.length == 0) {
      return MyWidgets.textLabelled(text: _baseData.text);
    }

    return Padding(
      padding: EdgeInsets.only(top: paddingL),
      child: Builder(
          builder: (BuildContext context) {
            return CustomScrollView(
              slivers: [
                  SliverOverlapInjector(handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context)),
                  SliverList(delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                    Restaurant restaurant = _baseData.restaurants[index];
                    return Hero(
                      tag: "${RESTAURANT_TAG}_${restaurant.id}",
                      child: RestaurantItemWidget(
                        restaurant: restaurant,
                        onRestaurantPressed: (){
                          Navigator.pushNamed(context, RouterName.restaurant_detail, arguments: restaurant);
                        },
                      ),
                    );
                  },
                  childCount: _baseData.restaurants.length,
                ))
              ],
            );
          }
      ),
    );
  }

}
