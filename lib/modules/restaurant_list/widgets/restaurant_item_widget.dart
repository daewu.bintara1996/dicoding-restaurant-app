import 'package:flutter/material.dart';
import 'package:restaurant_app/custom/app_themes.dart';
import 'package:restaurant_app/custom/constants.dart';
import 'package:restaurant_app/models/restaurant.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class RestaurantItemWidget extends StatelessWidget {
  final Restaurant restaurant;
  final Function() onRestaurantPressed;
  RestaurantItemWidget({this.restaurant, this.onRestaurantPressed});
  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: paddingL, vertical: paddingM),
      child: FlatButton(
        padding: EdgeInsets.all(0),
        onPressed: (){
          onRestaurantPressed();
        },
        child: Container(
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                child: FadeInImage.assetNetwork(
                  placeholder: "$ImagePlaceholderAsset",
                  image: "$IMG_URL_SMALL${restaurant?.pictureId}",
                  width: 100,
                  height: 70,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(width: paddingL),
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('${restaurant?.name}',
                      style: AppThemes.lightTheme.textTheme.subtitle1.copyWith(
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(height: paddingS),
                    Row(
                      children: [
                        Icon(Icons.location_on, color: Colors.grey, size: 16),
                        SizedBox(width: paddingS),
                        Text('${restaurant?.city}')
                      ],
                    ),
                    SizedBox(height: paddingS),
                    Row(
                      children: [
                        Icon(Icons.star, color: Colors.grey, size: 16),
                        SizedBox(width: paddingS),
                        Text('${restaurant?.rating}')
                      ],
                    ),

                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
