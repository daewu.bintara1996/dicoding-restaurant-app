import 'package:flutter/material.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

// SERVICE
const String BASE_URL_API = "https://restaurant-api.dicoding.dev/";
const String TOKEN_API = "12345";

// Assets
const String ImageLogoAsset = "assets/images/img_logo.png";
const String ImagePlaceholderAsset = "assets/images/img_placeholder.png";

// IMAGE URL
const String IMG_URL_SMALL = "https://restaurant-api.dicoding.dev/images/small/";
const String IMG_URL_MEDIUM = "https://restaurant-api.dicoding.dev/images/medium/";
const String IMG_URL_LARGE = "https://restaurant-api.dicoding.dev/images/large/";


// Color
const Color LightBackgroundColor = Color(0xFFF9F9F9);
const PrimaryColor = Colors.red;
const PrimaryVariant = Colors.amber;
const PrimaryColorLight = Color(0xFFEF9A9A);
const AccentColor = Colors.white;

const IconColor = AccentColor;
const TextColor =  Color(0xFF616161);
const ButtonColor =  PrimaryColor;
const TextButtonColor =  Colors.white;

// HERO TAG
const String RESTAURANT_TAG = "RES";

// Padding
const double paddingZero = 0.0;
const double paddingXS = 2.0;
const double paddingS = 4.0;
const double paddingM = 8.0;
const double paddingL = 16.0;
const double paddingXL = 32.0;
const double paddingXXL = 36.0;

// Margin
const double marginZero = 0.0;
const double marginXS = 2.0;
const double marginS = 4.0;
const double marginM = 8.0;
const double marginL = 16.0;
const double marginXL = 32.0;

// Spacing
const double spaceXS = 2.0;
const double spaceS = 4.0;
const double spaceM = 8.0;
const double spaceL = 16.0;
const double spaceXL = 32.0;