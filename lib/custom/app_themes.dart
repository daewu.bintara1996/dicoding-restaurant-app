import 'package:flutter/material.dart';
import 'constants.dart';
///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///


class AppThemes {
  AppThemes._();

  static String _fontFamily = "";

  // LIGHT THEME PRIVATE
  static final TextTheme _lightTextTheme = TextTheme(
    headline1: TextStyle(fontSize: 20.0, color: TextColor, fontFamily: _fontFamily),
    bodyText1: TextStyle(fontSize: 16.0, color: TextColor, fontFamily: _fontFamily),
    bodyText2: TextStyle(fontSize: 14.0, color: Colors.grey, fontFamily: _fontFamily),
    button: TextStyle(
        fontSize: 15.0, color: TextColor, fontWeight: FontWeight.w600, fontFamily: _fontFamily),
    headline6: TextStyle(fontSize: 16.0, color: TextColor, fontFamily: _fontFamily),
    subtitle1: TextStyle(fontSize: 16.0, color: TextColor, fontFamily: _fontFamily),
    caption: TextStyle(fontSize: 12.0, color: TextColor, fontFamily: _fontFamily),
  );

  // THE LIGHT THEME
  static final ThemeData lightTheme = ThemeData(
    brightness: Brightness.light,
    fontFamily: _fontFamily,
    primaryColor: PrimaryColor,
    accentColor: Colors.grey,
    backgroundColor: LightBackgroundColor,
    scaffoldBackgroundColor: LightBackgroundColor,
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: PrimaryColor,
    ),
    appBarTheme: AppBarTheme(
      color: PrimaryColor,
      iconTheme: IconThemeData(color: IconColor),
    ),
    colorScheme: ColorScheme.light(
      primary: PrimaryColor,
      primaryVariant: PrimaryVariant,
    ),
    snackBarTheme:
    SnackBarThemeData(backgroundColor: LightBackgroundColor),
    iconTheme: IconThemeData(
      color: IconColor,
    ),
    popupMenuTheme: PopupMenuThemeData(color: LightBackgroundColor),
    textTheme: _lightTextTheme,
  );

  // DARK THEME

}