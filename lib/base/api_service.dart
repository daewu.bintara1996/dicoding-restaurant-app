import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:restaurant_app/custom/constants.dart';
import 'package:restaurant_app/models/base_data.dart';

///
/// createdby Daewu Bintara
/// Friday, 30/10/20
///

class ApiService {
  Dio _dio = Dio();
  BaseData _data = BaseData();

  Map<String, dynamic> headers(){
    Map<String, dynamic> headers = new Map();
    headers[Headers.acceptHeader] = Headers.jsonContentType;
    headers[Headers.contentTypeHeader] = Headers.formUrlEncodedContentType;

    return headers;
  }

  Future<BaseData> getData({
      bool withToken = false,
      @required String endPoint,
      Map<String, dynamic> params}) async {
    var headers = this.headers();

    if(withToken){
      headers['X-Auth-Token'] = "$TOKEN_API";
    }

    if(kDebugMode) {
      log("GET ===> $BASE_URL_API$endPoint");
    }

    try {
      final response = await _dio.get("$BASE_URL_API$endPoint",
        options: Options(headers: headers),
        queryParameters: params);
      if(kDebugMode) {
        log("GET RESPONSE ===> ${response.data}");
      }
      _data = BaseData.fromJson(response.data);
      _data.text = _data.message;
      _data.status = !_data.error;
      return _data;
    } catch (e) {
      _data.status = true;
      _data.error = true;
      _data.text = "Error Network";
      _data.message = "Error Network";
      return _data;
    }
  }

  Future<BaseData> postData({
      bool withToken = false,
      @required String endPoint,
      @required Map<dynamic, dynamic> params}) async {
    var headers = this.headers();
    if(withToken){
      headers['X-Auth-Token'] = "$TOKEN_API";
    }

    if(kDebugMode) {
      log("POST ===> $BASE_URL_API$endPoint");
    }

    try {
      final response = await _dio.post("$BASE_URL_API$endPoint",
        options: Options(headers: headers),
        data: params);
      if(kDebugMode) {
        log("POST RESPONSE ===> ${response.data}");
      }
      _data = BaseData.fromJson(response.data);
      _data.text = _data.message;
      _data.status = !_data.error;
      return _data;
    } catch (e) {
      _data.status = false;
      _data.error = true;
      _data.text = "Error Network";
      _data.message = "Error Network";
      return _data;
    }
  }

}