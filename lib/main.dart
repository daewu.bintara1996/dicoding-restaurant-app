import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:restaurant_app/custom/app_themes.dart';
import 'package:restaurant_app/routes/routes.dart';
import 'custom/constants.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: LightBackgroundColor
    ));
    return MaterialApp(
      theme: AppThemes.lightTheme,
      onGenerateRoute: Pages.generateRoute,
      initialRoute: RouterName.splash,
      debugShowCheckedModeBanner: false,
    );
  }
}
