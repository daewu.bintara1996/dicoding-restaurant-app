import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant_app/custom/constants.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class MyWidgets {

  static circleLoading() {
    return Center(
      child: Container(
        height: 35,
        width: 35,
        padding: EdgeInsets.all(8),
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(PrimaryColor),
          strokeWidth: 2,
        ),
      ),
    );
  }

  static textLabelled({String text}) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: PrimaryColorLight,
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Text(
          text,
          style: TextStyle(color: Colors.black),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

}