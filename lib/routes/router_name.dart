///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class RouterName {
  static const String splash = "/splash";
  static const String restaurant_list = "/restaurant_list";
  static const String restaurant_detail = "/restaurant_detail";
  static const String restaurant_search = "/restaurant_search";
}