import 'package:flutter/material.dart';
import 'package:restaurant_app/models/base_data.dart';
import 'package:restaurant_app/models/restaurant.dart';
import 'package:restaurant_app/modules/restaurant_detail/restaurant_detail_screen.dart';
import 'package:restaurant_app/modules/restaurant_list/restaurant_list_screen.dart';
import 'package:restaurant_app/modules/search_restaurant/search_restaurant_screen.dart';
import 'package:restaurant_app/modules/splash/splash_screen.dart';

import 'routes.dart';

///
/// createdby Daewu Bintara
/// Monday, 26/10/20
///

class Pages{
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch(settings.name){
      case RouterName.splash :
        return MaterialPageRoute(
            builder: (_) => SplashScreen()
        );
        break;
      case RouterName.restaurant_list :
        return MaterialPageRoute(
            builder: (_) => RestaurantListScreen()
        );
        break;
      case RouterName.restaurant_detail :
        var data = settings.arguments as Restaurant;
        return MaterialPageRoute(
            builder: (_) => RestaurantDetailScreen(data)
        );
        break;
      case RouterName.restaurant_search :
        return MaterialPageRoute(
            builder: (_) => SearchRestaurantScreen()
        );
        break;
    }

  }

}